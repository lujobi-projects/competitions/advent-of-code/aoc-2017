#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 1;

#[derive(Debug)]
pub struct Coordinate {
    x: u64,
    y: u64,
}

pub fn part1() -> i64 {
    let input = parser::CoordinatesParser::new()
        .parse(&get_input!())
        .unwrap();
    print!("{:?}", input);
    0
}

pub fn part2() -> i64 {
    let input = parser::CoordinatesParser::new()
        .parse(&get_input!())
        .unwrap();
    print!("{:?}", input);
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 0);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 0);
    }
}
