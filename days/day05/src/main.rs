use utils::*;

const CURRENT_DAY: u8 = 5;

pub fn part1() -> i64 {
    let mut input = read_input!("\n", i64);

    let mut steps = 0;
    let mut pos = 0;

    while pos < input.len() as i64 {
        let new_pos = pos + input[pos as usize];
        input[pos as usize] += 1;
        pos = new_pos;
        steps += 1;
    }

    steps
}

pub fn part2() -> i64 {
    let mut input = read_input!("\n", i64);

    let mut steps = 0;
    let mut pos = 0;

    while pos < input.len() as i64 {
        let offset = input[pos as usize];
        let new_pos = pos + offset;

        if offset >= 3 {
            input[pos as usize] -= 1;
        } else {
            input[pos as usize] += 1;
        }

        pos = new_pos;
        steps += 1;
    }

    steps
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 336905);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 21985262);
    }
}
