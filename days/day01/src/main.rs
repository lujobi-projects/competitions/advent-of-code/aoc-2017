use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 1;

pub fn part1() -> i64 {
    let input = read_input!();

    let mut sum = 0;

    for (fst, snd) in input.iter().tuple_windows() {
        if fst == snd {
            sum += snd.to_digit(10).unwrap() as i64;
        }
    }

    if input[0] == input[input.len() - 1] {
        sum += input[0].to_digit(10).unwrap() as i64;
    }

    sum
}
pub fn part2() -> i64 {
    let input = read_input!();

    let len = input.len();
    let mut sum = 0;

    for i in 0..len {
        let fst = input[i];
        let snd = input[(i + len / 2) % len];

        if fst == snd {
            sum += fst.to_digit(10).unwrap() as i64;
        }
    }
    sum
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1150);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 1064);
    }
}
