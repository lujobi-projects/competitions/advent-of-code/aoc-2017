#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::collections::{HashMap, HashSet};

use utils::*;

const CURRENT_DAY: u8 = 7;

#[derive(Debug)]
pub enum ProgramTree {
    Leaf(String, i64),
    Node(String, i64, Vec<String>),
}

#[derive(Debug, Hash, Eq, PartialEq)]
pub enum AST {
    Leaf(i64),
    Node(i64, Vec<String>),
}

pub fn part1() -> String {
    let parser = parser::ProgramTreeParser::new();
    let input = read_input!("\n", String)
        .iter()
        .map(|x| parser.parse(x).unwrap())
        .collect::<Vec<_>>();

    let mut seen_names = HashSet::new();
    let mut seen_children = HashSet::new();

    for program in input.iter() {
        match program {
            ProgramTree::Leaf(name, _) => {
                seen_names.insert(name);
            }
            ProgramTree::Node(name, _, children) => {
                seen_names.insert(name);
                for child in children {
                    seen_children.insert(child);
                }
            }
        }
    }

    for name in seen_names.iter() {
        if !seen_children.contains(name) {
            return (*name).clone();
        }
    }

    panic!("No root found");
}

fn find_weight(tree: &HashMap<String, AST>, name: &String) -> Result<i64, i64> {
    match tree.get(name).unwrap() {
        AST::Node(weight, children) => {
            let mut weights = HashMap::new();
            let mut sum = 0;
            for child in children {
                let child_weight = find_weight(tree, child)?;
                weights.entry(child_weight).or_insert((0, child)).0 += 1;
                sum += child_weight;
            }
            if weights.len() > 1 {
                let mut weights = weights.iter().collect::<Vec<_>>();
                weights.sort_by(|a, b| (a.1).0.cmp(&(b.1).0));
                let child_weight = match tree.get((weights[0].1).1).unwrap() {
                    AST::Leaf(weight) => weight,
                    AST::Node(weight, _) => weight,
                };
                return Err(child_weight + weights[1].0 - weights[0].0);
            }

            Ok(sum + weight)
        }
        AST::Leaf(weight) => Ok(*weight),
    }
}

pub fn part2() -> i64 {
    let parser = parser::ProgramTreeParser::new();
    let input = read_input!("\n", String)
        .iter()
        .map(|x| parser.parse(x).unwrap())
        .collect::<Vec<_>>();

    let mut tree: HashMap<String, AST> = HashMap::new();

    for program in input.iter() {
        match program {
            ProgramTree::Leaf(name, weight) => {
                tree.insert(name.clone(), AST::Leaf(*weight));
            }
            ProgramTree::Node(name, weight, children) => {
                tree.insert(name.clone(), AST::Node(*weight, children.to_vec()));
            }
        }
    }

    let root = part1();

    find_weight(&tree, &root).unwrap_err()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2()); // 1786 too high
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), "ahnofa");
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 802);
    }
}
