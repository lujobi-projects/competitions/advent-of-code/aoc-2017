use std::collections::HashSet;

use utils::*;

const CURRENT_DAY: u8 = 6;

pub fn redistribute(current: &[i64]) -> Vec<i64> {
    let mut current = current.to_owned();
    let max = current.iter().max().unwrap();
    let max_index = current.iter().position(|x| x == max).unwrap();
    let mut to_distribute = *max;
    current[max_index] = 0;
    let mut index = max_index + 1;
    let len = current.len();
    while to_distribute > 0 {
        current[index % len] += 1;
        to_distribute -= 1;
        index += 1;
    }
    current
}

pub fn part1() -> i64 {
    let input = read_input!("\t", i64);

    let mut current = input.clone();
    let mut seen = HashSet::new();
    seen.insert(input.clone());

    loop {
        current = redistribute(&current);
        if seen.contains(&current) {
            break;
        }
        seen.insert(current.clone());
    }

    seen.len() as i64
}

pub fn part2() -> i64 {
    let input = read_input!("\t", i64);

    let mut current = input.clone();
    let mut seen = HashSet::new();
    seen.insert(input.clone());

    loop {
        current = redistribute(&current);
        if seen.contains(&current) {
            break;
        }
        seen.insert(current.clone());
    }

    let target = current.clone();
    let mut steps = 0;

    loop {
        current = redistribute(&current);
        steps += 1;
        if current == target {
            break;
        }
    }

    steps
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 5042);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 1086);
    }
}
