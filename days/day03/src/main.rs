use utils::*;

const CURRENT_DAY: u8 = 3;

pub fn part1() -> i64 {
    // let input = read_input!(u64);

    // let mut coords = (0, 0);

    // let dirs = [(1, 0), (0, 1), (-1, 0), (0, -1)];

    // let mut pos = 1;

    // while pos != input {
    //     let dir = dirs[(pos - 1) % 4];
    //     let steps = (pos - 1) / 4 + 1;

    //     coords.0 += dir.0 * steps as i64;
    //     coords.1 += dir.1 * steps as i64;

    //     pos += 1;
    // }

    // coords.0.abs() + coords.1.abs()
    0
}
pub fn part2() -> i64 {
    // let input = read_input!(i64);
    // let mut sum = 0;
    // let mut pos = 1;

    // while sum < input {
    //     pos = sum;
    //     sum += pos;
    // }

    // sum
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 0);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 0);
    }
}
