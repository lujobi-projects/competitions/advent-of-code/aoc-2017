use utils::*;

const CURRENT_DAY: u8 = 2;

pub fn part1() -> i64 {
    let input = read_input!("\n", "\t", i64);
    input
        .iter()
        .map(|row| (row.iter().max().unwrap() - row.iter().min().unwrap()).abs())
        .sum()
}
pub fn part2() -> i64 {
    let input = read_input!("\n", "\t", i64);
    input
        .iter()
        .map(|row| {
            for (i, fst) in row.iter().enumerate() {
                for snd in row.iter().skip(i + 1) {
                    if fst % snd == 0 {
                        return fst / snd;
                    } else if snd % fst == 0 {
                        return snd / fst;
                    }
                }
            }
            0
        })
        .sum()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 30994);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 233);
    }
}
