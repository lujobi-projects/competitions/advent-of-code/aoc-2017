use std::collections::HashSet;
use std::iter::FromIterator;

use utils::*;

const CURRENT_DAY: u8 = 4;

pub fn part1() -> i64 {
    let input = read_input!("\n", " ", String);
    input
        .iter()
        .filter(|phrase| phrase.len() == HashSet::<String>::from_iter(phrase.iter().cloned()).len())
        .count() as i64
}
pub fn part2() -> i64 {
    let input = read_input!("\n", " ", String);

    input
        .iter()
        .filter(|phrase| {
            phrase.len()
                == HashSet::<[u8; 26]>::from_iter(phrase.iter().cloned().map(|word| {
                    let mut m = [0; 26];
                    for c in word.chars() {
                        m[(c as u8 - b'a') as usize] += 1;
                    }
                    m
                }))
                .len()
        })
        .count() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 386);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 208);
    }
}
